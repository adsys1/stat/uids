package main

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
	"net"
)

var (
	address = pflag.String("addr", ":8098", "")
)

func main() {
	pflag.Parse()

	addr, err := net.ResolveUDPAddr("udp", *address)
	if err != nil {
		log.Fatal().Err(err)
	}

	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		log.Fatal().Err(err)
	}

	defer func() {
		_ = conn.Close()
	}()

	log.Info().Str("address", addr.String()).Msg("udp server started")

	buf := make([]byte, 1024)

	for {
		n, addr, err := conn.ReadFromUDP(buf)
		if err != nil {
			log.Err(err).Str("address", addr.String())
			continue
		}

		log.Print("Received " + string(buf[0:n]))
	}
}
